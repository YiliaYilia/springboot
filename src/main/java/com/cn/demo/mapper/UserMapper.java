package com.cn.demo.mapper;



import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

import com.cn.demo.doman.User;

/**
 * 功能描述：访问数据库的接口
 *
 *
 */
@Mapper
public interface UserMapper {
	
	
	//推荐使用#{}取值，不要用${},因为存在注入的风险
	 @Insert("INSERT INTO user(name,phone,create_time,age) VALUES(#{name}, #{phone}, #{createTime},#{age})")
	 @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")  
	 //keyProperty java对象的属性；keyColumn表示数据库的字段
	 int insert(User user);

}