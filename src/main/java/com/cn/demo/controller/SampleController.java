package com.cn.demo.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cn.demo.doman.User;
@EnableAutoConfiguration
@RestController  
public class SampleController {  
  //requestMapping,ResponseBody 是springmvc的注解
    @RequestMapping("/")  
    @ResponseBody  
    String home() {  
        return "Hello World!";  
    }  
    @RequestMapping("/test")  
    public Map<String,String> testMap(){
    	Map<String,String>map=new HashMap<>();
    	map.put("1", "yilia");
    	return map;
    }
    @RequestMapping("/testjson")  
    public Object testJson(){
    	 
    	//return new User(111,"yilia","100100",new Date());
    	return null;
    }
}  
