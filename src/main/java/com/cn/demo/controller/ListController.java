package com.cn.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@PropertySource("classpath:application.properties")
@Controller
public class ListController {
	@RequestMapping(value = "/api/list/demo")  
	public Object index() {
		List<String> person=new ArrayList<>();
        person.add("jackie");    
        person.add("peter");     
        person.add("annie");    
        person.add("martin");    
        person.add("marry");    
         
     
		
		return "person";
	}
}
