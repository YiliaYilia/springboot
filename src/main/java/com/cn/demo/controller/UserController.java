package com.cn.demo.controller;


import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cn.demo.doman.JsonData;
import com.cn.demo.doman.User;
import com.cn.demo.mapper.UserMapper;
import com.cn.demo.service.UserService;

/**
 */
@RestController
@RequestMapping("/api/v1/user")
public class UserController {
	
	
	@Autowired
	private UserService userService;

	@Autowired
	private UserMapper userMapper;
	
	
	/**
	 * 功能描述: user 保存接口
	 * @return
	 */
	@GetMapping("add")
	public Object add(){
		
		User user = new User();
		user.setAge(11);
		user.setCreateTime(new Date());
		user.setName("juanjuan");
		user.setPhone("15021666943");
		int id = userService.add(user);
		
       return JsonData.buildSuccess(id);
	}
	
}
