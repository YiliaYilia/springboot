package com.cn.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cn.demo.doman.ServerSettings;


@Controller
@RequestMapping("/tyhmeleaf")
public class ThymeleafController {

	
	@Autowired
	private ServerSettings setting;

	
	@GetMapping("hello")
	public String index(){
		
		
		return "index";  //不用加后缀，在配置文件里面已经指定了后缀
	}
	@GetMapping("velocitylayout")
	public String velocity(){
		
		
		return "news";  //不用加后缀，在配置文件里面已经指定了后缀
	}
	 
	@GetMapping("vm" )
	public ModelAndView IndexNew () {
	 
	      return new ModelAndView("new.vm") ;
	  
	}
	

	@GetMapping("info")
	public String admin(ModelMap modelMap){
		
		modelMap.addAttribute("setting", setting);
		
		return "admin/info";  //不用加后缀，在配置文件里面已经指定了后缀
	}
}
