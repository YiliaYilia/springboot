package com.cn.demo;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootConfiguration
@MapperScan("com.cn.demo.mapper.UserMapper")
@EnableAutoConfiguration
@ComponentScan
@ServletComponentScan
//@EnableScheduling
public class MyApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(MyApplication.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(MyApplication.class, args);
    }

}
//public class MyApplication {
//	public static void main(String[] args) throws Exception {  
//        SpringApplication.run(MyApplication.class, args);  
//        System.out.println("MyApplication==============");
//    } 
//	 /**  
//     * 文件上传配置  
//     * @return  
//     */  
//    @Bean  
//    public MultipartConfigElement multipartConfigElement() {  
//        MultipartConfigFactory factory = new MultipartConfigFactory();  
//        //文件最大  
//        factory.setMaxFileSize("10240KB"); //KB,MB  
//        /// 设置总上传数据总大小  
//        factory.setMaxRequestSize("102400KB");  
//        return factory.createMultipartConfig();  
//    }  
//  
// 
//}
